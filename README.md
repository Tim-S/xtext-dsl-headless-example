# Deploy your Xtext Dsl to a Headless Plain-Java Environment with Gradle in 5 minutes

## Create an Gradle-Enabled Xtext-Project 
Create a new Xtext Project and make sure you choose Gradle as your Preferred Build System
![alt text](EnableGradleInXtextProjectWizard.PNG "Logo Title Text 1")
## Generate a Java-Main
To make Xtext create a [Main.java](#) we add
```java
language = StandardLanguage {
			//...
			generator = {
				generateJavaMain = true
			}
		}
```
to [GenerateMyDsl.mwe2](#).

## Configure Gradle to Create a Standalone JAR
Now that we a have a Main-Class generated, we need to configure gradle to build a Standalone JAR. We will use the  [Shadow Plugin](https://github.com/johnrengelman/shadow) for Gradle provided by John Engelman.

### 1) Add the Shadowplugin as a dependency
```gradle
buildscript {
	dependencies {
		//...
		classpath "com.github.jengelman.gradle.plugins:shadow:2.0.1"
	}
}
```
to [org.xtext.example.parent/build.gradle](#)

### 2) Configure our Main Class

```gradle
apply plugin: 'java'
apply plugin: 'application'
apply plugin: 'com.github.johnrengelman.shadow'
mainClassName = 'org.xtext.example.mydsl.generator.Main'
shadowJar { 
 	transform(com.github.jengelman.gradle.plugins.shadow.transformers.AppendingTransformer){
 		resource = 'plugin.properties'
 	}
}
```
in [org.xtext.example.mydsl/build.gradle]().
Note that that we configured the AppendingTransformer to concatenate all plugin.properties, since otherwise the properties files will override each other and EMF won't be able to find the properties containing the
substitutions for the strings.

Which will give you an error similar to 
```bash
... The string resource '_UI_DiagnosticRoot_diagnostic' could not be located ...
```

### 3) Build Standalone JAR
```bash
cd org.xtext.example.mydsl.parent
./gradlew shadowJar

```
Once the build completes you should find the standalone-Jar in [org.xtext.example.mydsl/build/libs](#).

You should be able to run the standalone-generator with
```bash
java -jar org.xtext.example.mydsl-1.0.0-SNAPSHOT-all.jar some.mydsl
```

### 4) Distributing the Standalone JAR
Since we use the application plugin, the shadow plugin will 
also configure distribution tasks. It will create shadowDistZip and shadowDistTar which creates Zip and Tar distributions respectively. Each distribution will contain the standalone JAR file along with the necessary start scripts to launch the application.

After running
```bash
./gradlew shadowDistZip
```
you should find a [org.xtext.example.mydsl-shadow-1.0.0-SNAPSHOT.zip](#)
in
[org.xtext.example.mydsl/build/distributions](#).


After extracition you can launch generation via the start scripts

Linux/Bash: 
```bash
cd org.xtext.example.mydsl-shadow-1.0.0-SNAPSHOT/bin
./org.xtext.example.mydsl some.mydsl
```

Windows:
```
cd org.xtext.example.mydsl-shadow-1.0.0-SNAPSHOT/bin
org.xtext.example.mydsl.bat some.mydsl
```
